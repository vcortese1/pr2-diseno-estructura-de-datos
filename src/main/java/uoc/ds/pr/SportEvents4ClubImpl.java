package uoc.ds.pr;

import edu.uoc.ds.adt.nonlinear.Dictionary;
import edu.uoc.ds.adt.nonlinear.DictionaryAVLImpl;
import edu.uoc.ds.adt.nonlinear.HashTable;
import edu.uoc.ds.adt.nonlinear.PriorityQueue;
import edu.uoc.ds.adt.nonlinear.graphs.DirectedEdge;
import edu.uoc.ds.adt.nonlinear.graphs.DirectedGraph;
import edu.uoc.ds.adt.nonlinear.graphs.DirectedGraphImpl;
import edu.uoc.ds.adt.nonlinear.graphs.Vertex;
import edu.uoc.ds.adt.sequential.LinkedList;
import edu.uoc.ds.adt.sequential.List;
import edu.uoc.ds.traversal.Iterator;
import uoc.ds.pr.exceptions.*;
import uoc.ds.pr.model.*;
import uoc.ds.pr.util.OrderedVector;

import java.time.LocalDate;

public class SportEvents4ClubImpl implements SportEvents4Club {

    private Dictionary<String, Player> players;
    private HashTable<String, OrganizingEntity> organizingEntities;
    private PriorityQueue<File> files;
    private int totalFiles;
    private int rejectedFiles;
    private Dictionary<String, SportEvent> sportEvents;
    private SportEvent mostAttendersEvent;
    private Player mostActivePlayer;
    private OrderedVector<SportEvent> bestSportEvents;
    private OrderedVector<OrganizingEntity> mostAttendersOrganizingEntities;
    private HashTable<String, Worker> workers;
    private Role[] roles;
    private int numRoles;

    private DirectedGraph<Player, String> socialNetwork;

    public SportEvents4ClubImpl() {
        players = new DictionaryAVLImpl<>();
        organizingEntities = new HashTable<>();
        files = new PriorityQueue<>(File.FILE_PRIORITY);
        sportEvents = new DictionaryAVLImpl<>();
        totalFiles = 0;
        rejectedFiles = 0;
        mostActivePlayer = null;
        mostAttendersEvent=null;
        bestSportEvents = new OrderedVector<>(MAX_NUM_SPORT_EVENTS, SportEvent.CMP_V);
        mostAttendersOrganizingEntities = new OrderedVector<>(MAX_ORGANIZING_ENTITIES_WITH_MORE_ATTENDERS, OrganizingEntity.CMP_MOST_ATTENDERS);
        workers = new HashTable<>();
        roles = new Role[MAX_ROLES];
        numRoles = 0;
        socialNetwork = new DirectedGraphImpl<>();
    }

    @Override
    public void addPlayer(String playerId, String name, String surname, LocalDate dateOfBirth) {
        Player u = getPlayer(playerId);
        if (u != null) {
            u.setName(name);
            u.setSurname(surname);
            u.setBirthday(dateOfBirth);
        } else {
            u = new Player(playerId, name, surname, dateOfBirth);
            addUser(u);
        }
    }

    public void addUser(Player player) {
        players.put(player.getId(),player);
        socialNetwork.newVertex(player);
    }

    @Override
    public void addOrganizingEntity(String organizationId, String name, String description) {
        OrganizingEntity organizingEntity = getOrganizingEntity(organizationId);
        if (organizingEntity != null) {
            organizingEntity.setName(name);
            organizingEntity.setDescription(description);
        } else {
            organizingEntity = new OrganizingEntity(organizationId, name, description);
            organizingEntities.put(organizationId,organizingEntity);
        }
    }

    @Override
    public void addFile(String id, String eventId, String orgId, String description, Type type, byte resources, int max, LocalDate startDate, LocalDate endDate) throws OrganizingEntityNotFoundException {
        if (!organizingEntities.containsKey(orgId)) {
            throw new OrganizingEntityNotFoundException();
        }
        OrganizingEntity organization = getOrganizingEntity(orgId);
        files.add(new File(id, eventId, description, type, startDate, endDate, resources, max, organization));
        totalFiles++;
    }

    @Override
    public File updateFile(Status status, LocalDate date, String description) throws NoFilesException {
        File file = files.poll();
        if (file  == null) {
            throw new NoFilesException();
        }

        file.update(status, date, description);
        if (file.isEnabled()) {
            SportEvent sportEvent = file.newSportEvent();
            sportEvents.put(sportEvent.getEventId(), sportEvent);
        }
        else {
            rejectedFiles++;
        }

        return file;
    }

    @Override
    public void signUpEvent(String playerId, String eventId) throws PlayerNotFoundException, SportEventNotFoundException, LimitExceededException {
        Player player = getPlayer(playerId);
        if (player == null) {
            throw new PlayerNotFoundException();
        }

        SportEvent sportEvent = getSportEvent(eventId);
        if (sportEvent == null) {
            throw new SportEventNotFoundException();
        }

        player.addEvent(sportEvent);
        if (!sportEvent.isFull()) {
            sportEvent.addEnrollment(player, eventId);
        }
        else {
            sportEvent.addEnrollmentAsSubtitute(player, eventId);
            throw new LimitExceededException();
        }
        updateMostActivePlayer(player);
    }

    private void updateMostActivePlayer(Player player) {
        if (mostActivePlayer == null) {
            mostActivePlayer = player;
        }
        else if (player.numSportEvents() > mostActivePlayer.numSportEvents()) {
            mostActivePlayer = player;
        }
    }

    @Override
    public double getRejectedFiles() {
        return (double) rejectedFiles / totalFiles;
    }

    @Override
    public Iterator<SportEvent> getSportEventsByOrganizingEntity(String organizationId) throws NoSportEventsException {
        OrganizingEntity organizingEntity = getOrganizingEntity(organizationId);

        if (organizingEntity==null || !organizingEntity.hasActivities()) {
            throw new NoSportEventsException();
        }
        return organizingEntity.sportEvents();
    }

    @Override
    public Iterator<SportEvent> getAllEvents() throws NoSportEventsException {
        Iterator<SportEvent> it = sportEvents.values();
        if (!it.hasNext()) throw new NoSportEventsException();
        return it;
    }

    @Override
    public Iterator<SportEvent> getEventsByPlayer(String playerId) throws NoSportEventsException {
        Player player = getPlayer(playerId);
        if (player==null || !player.hasEvents()) {
            throw new NoSportEventsException();
        }
        Iterator<SportEvent> it = player.getEvents();

        return it;
    }

    @Override
    public void addRating(String playerId, String eventId, Rating rating, String message) throws SportEventNotFoundException, PlayerNotFoundException, PlayerNotInSportEventException {
        SportEvent sportEvent = getSportEvent(eventId);
        if (sportEvent == null) {
            throw new SportEventNotFoundException();
        }

        Player player = getPlayer(playerId);
        if (player == null) {
            throw new PlayerNotFoundException();
        }

        if (!player.isInSportEvent(eventId)) {
            throw new PlayerNotInSportEventException();
        }

        uoc.ds.pr.model.Rating ratingObj = sportEvent.addRating(rating, message, player);
        player.addRating(ratingObj);

        // Update bestSportEvent OrderedVector
        bestSportEvents.delete(sportEvent);
        bestSportEvents.update(sportEvent);
    }

    @Override
    public Iterator<uoc.ds.pr.model.Rating> getRatingsByEvent(String eventId) throws SportEventNotFoundException, NoRatingsException {
        SportEvent sportEvent = getSportEvent(eventId);
        if (sportEvent  == null) {
            throw new SportEventNotFoundException();
        }

        if (!sportEvent.hasRatings()) {
            throw new NoRatingsException();
        }

        return sportEvent.ratings();
    }

    @Override
    public Player mostActivePlayer() throws PlayerNotFoundException {
        if (mostActivePlayer == null) {
            throw new PlayerNotFoundException();
        }
        return mostActivePlayer;
    }

    @Override
    public SportEvent bestSportEvent() throws SportEventNotFoundException {
        if (bestSportEvents.size() == 0) {
            throw new SportEventNotFoundException();
        }

        return bestSportEvents.elementAt(0);
    }

    @Override
    public void addRole(String roleId, String description) {
        Role role = getRole(roleId);
        if (role != null) {
            role.setDescription(description);
        } else {
            role = new Role(roleId, description);
            roles[numRoles] = role;
            numRoles++;
        }
    }

    @Override
    public void addWorker(String dni, String name, String surname, LocalDate birthDay, String roleId) {
        Worker worker = getWorker(dni);
        Role role = getRole(roleId);
        if(worker != null){
            worker.setName(name);
            worker.setSurname(surname);
            worker.setBirthday(birthDay);
            Role previousRole = worker.getRole();
            if(worker.setRole(role)) {
                role.replaceWorkerRole(worker, previousRole);
            }
        } else {
            worker = new Worker(dni, name, surname, birthDay, role);
            workers.put(dni,worker);
            role.addWorkerToRole(worker);
        }
    }

    @Override
    public void assignWorker(String dni, String eventId) throws WorkerNotFoundException, WorkerAlreadyAssignedException, SportEventNotFoundException {
        SportEvent sportEvent = getSportEvent(eventId);
        if(sportEvent == null){
            throw new SportEventNotFoundException();
        }

        Worker worker = getWorker(dni);
        if(worker == null){
            throw new WorkerNotFoundException();
        }

        if(sportEvent.hasWorker(dni)){
            throw new WorkerAlreadyAssignedException();
        } else {
            sportEvent.addWorker(worker);
        }
    }

    @Override
    public Iterator<Worker> getWorkersBySportEvent(String eventId) throws SportEventNotFoundException, NoWorkersException {
        SportEvent sportEvent = getSportEvent(eventId);
        if(sportEvent == null){
            throw new SportEventNotFoundException();
        }

        if(!sportEvent.hasWorkers()){
            throw new NoWorkersException();
        } else {
            return sportEvent.getWorkers();
        }
    }

    @Override
    public Iterator<Worker> getWorkersByRole(String roleId) throws NoWorkersException {
        Role role = getRole(roleId);
        if(!role.hasAssignedWorkers()){
            throw new NoWorkersException();
        } else {
            return role.getAssignedWorkers();
        }
    }

    @Override
    public Level getLevel(String playerId) throws PlayerNotFoundException {
        Player player = getPlayer(playerId);
        if(player == null){
            throw new PlayerNotFoundException();
        }
        return player.getLevel();

    }

    @Override
    public Iterator<Enrollment> getSubstitutes(String eventId) throws SportEventNotFoundException, NoSubstitutesException {
        SportEvent sportEvent = getSportEvent(eventId);
        if(sportEvent == null){
            throw new SportEventNotFoundException();
        }

        if(sportEvent.getNumSubstitutes() == 0){
            throw new NoSubstitutesException();
        } else {
            return sportEvent.getSubtitutesEnrollments();
        }

    }

    @Override
    public void addAttender(String phone, String name, String eventId) throws AttenderAlreadyExistsException, SportEventNotFoundException, LimitExceededException {
        SportEvent sportEvent = getSportEvent(eventId);
        if(sportEvent == null){
            throw new SportEventNotFoundException();
        }
        if(sportEvent.hasAttender(phone)){
            throw new AttenderAlreadyExistsException();
        }
        if(sportEvent.isFull()){
            throw new LimitExceededException();
        } else {
            Attender attender = new Attender(phone, name);
            sportEvent.addAttender(attender);
            updateMostAttendersEvent(sportEvent);
            updateMostAttendersOrganizingEntities(sportEvent.getOrganizingEntity());
        }
    }

    public void updateMostAttendersOrganizingEntities(OrganizingEntity organizingEntity){
        mostAttendersOrganizingEntities.delete(organizingEntity);
        mostAttendersOrganizingEntities.update(organizingEntity);
    }

    private void updateMostAttendersEvent(SportEvent sportEvent) {
        if (mostAttendersEvent == null) {
            mostAttendersEvent = sportEvent;
        }
        else if (sportEvent.numAttenders() > mostAttendersEvent.numAttenders()) {
            mostAttendersEvent = sportEvent;
        }
    }

    @Override
    public Attender getAttender(String phone, String sportEventId) throws SportEventNotFoundException, AttenderNotFoundException {
        SportEvent sportEvent = getSportEvent(sportEventId);
        if (sportEvent == null){
            throw new SportEventNotFoundException();
        }
        if(!sportEvent.hasAttender(phone)){
            throw new AttenderNotFoundException();
        } else {
            return sportEvent.getAttender(phone);
        }
    }

    @Override
    public Iterator<Attender> getAttenders(String sportEventId) throws SportEventNotFoundException, NoAttendersException {
        SportEvent sportEvent = getSportEvent(sportEventId);
        if (sportEvent == null){
            throw new SportEventNotFoundException();
        }
        if(!sportEvent.hasAttenders()){
            throw new NoAttendersException();
        } else {
            return sportEvent.getAttenders();
        }
    }

    @Override
    public Iterator<OrganizingEntity> best5OrganizingEntities() throws NoAttendersException {
        if(mostAttendersOrganizingEntities.isEmpty()){
            throw new NoAttendersException();
        }
        return mostAttendersOrganizingEntities.values();
    }

    @Override
    public SportEvent bestSportEventByAttenders() throws NoSportEventsException {
        if(mostAttendersEvent == null){
            throw new NoSportEventsException();
        }
        return mostAttendersEvent;
    }

    @Override
    public void addFollower(String playerId, String playerFollowerId) throws PlayerNotFoundException {
        Player targetPlayer = getPlayer(playerId), followerPlayer = getPlayer(playerFollowerId);
        if(targetPlayer == null || followerPlayer == null){
            throw new PlayerNotFoundException();
        }
        Vertex<Player> vTargetPlayer, vfollowerPlayer;
        vTargetPlayer = socialNetwork.getVertex(targetPlayer);
        vfollowerPlayer = socialNetwork.getVertex(followerPlayer);
        // Si el follower no es ya seguidor del otro jugador debems crear la relación
        if(socialNetwork.getEdge(vfollowerPlayer, vTargetPlayer) == null){
            socialNetwork.newEdge(vfollowerPlayer,vTargetPlayer);
            targetPlayer.addFollower();
            followerPlayer.addFollowing();
        }
    }

    @Override
    public Iterator<Player> getFollowers(String playerId) throws PlayerNotFoundException, NoFollowersException {
        Player player = getPlayer(playerId);
        if(player == null){
            throw new PlayerNotFoundException();
        }
        if(player.getNumFollowers() == 0){
            throw new NoFollowersException();
        } else {
            Vertex<Player> vPlayer = socialNetwork.getVertex(player);
            List<Player> followers = new LinkedList<>();
            for (Iterator i = socialNetwork.edgedWithDestA(vPlayer); i.hasNext(); ){
                DirectedEdge<String, Player> follower = (DirectedEdge<String, Player>) i.next();
                followers.insertEnd(follower.getVertexSrc().getValue());
            }
            return followers.values();
        }

    }

    @Override
    public Iterator<Player> getFollowings(String playerId) throws PlayerNotFoundException, NoFollowingException {
        Player player = getPlayer(playerId);
        if(player == null){
            throw new PlayerNotFoundException();
        }
        if(player.getNumFollowing() == 0){
            throw new NoFollowingException();
        } else {
            Vertex<Player> vPlayer = socialNetwork.getVertex(player);
            List<Player> following = new LinkedList<>();
            for (Iterator i = socialNetwork.edgesWithSource(vPlayer); i.hasNext(); ){
                DirectedEdge<String, Player> follower = (DirectedEdge<String, Player>) i.next();
                following.insertEnd(follower.getVertexDst().getValue());
            }
            return following.values();
        }
    }

    @Override
    public Iterator<Player> recommendations(String playerId) throws PlayerNotFoundException, NoFollowersException {
       Player player = getPlayer(playerId);
       if(player == null){
           throw new PlayerNotFoundException();
       }
       if(player.getNumFollowers() == 0){
           throw new NoFollowersException();
       } else {
           Iterator<Player> playerFollowers = getFollowers(playerId);
           List<Player> recomendedPlayers = new LinkedList<>();
           for(Iterator<Player> i = playerFollowers; i.hasNext();){
                Player follower = i.next();
                if(follower.getNumFollowers() > 0){
                    for(Iterator<Player> i2 = getFollowers(follower.getId()); i2.hasNext();){
                        Player p = i2.next();
                        boolean cond1 = p.isFollower(getFollowers(playerId)),
                                cond2 = p.getId().equals(playerId),
                                cond3 = p.isFollower(recomendedPlayers.values());
                        if(!cond1 && !cond2 && !cond3)
                            recomendedPlayers.insertEnd(p);
                    }
                }
           }
           return recomendedPlayers.values();
       }
    }

    @Override
    public Iterator<Post> getPosts(String playerId) throws PlayerNotFoundException, NoPostsException {
        Player player = getPlayer(playerId);
        if(player == null){
            throw new PlayerNotFoundException();
        }

        try {
            LinkedList<Post> posts = new LinkedList<>();
            Iterator<Player> playersFollowing = getFollowings(playerId);
            for(Iterator<Player> i = playersFollowing; i.hasNext();){
                Player p = i.next();
                for(Iterator<Post> i2 = p.getPosts(); i2.hasNext();){
                    Post post = i2.next();
                    posts.insertEnd(post);
                }
            }
            if(posts.isEmpty()) throw new NoPostsException();
            else return posts.values();
        } catch (NoFollowingException e) {
            throw new NoPostsException();
        }
    }

    @Override
    public int numPlayers() {
        return players.size();
    }

    @Override
    public int numOrganizingEntities() {
        return organizingEntities.size();
    }

    @Override
    public int numFiles() {
        return totalFiles;
    }

    @Override
    public int numRejectedFiles() {
        return rejectedFiles;
    }

    @Override
    public int numPendingFiles() {
        return files.size();
    }

    @Override
    public int numSportEvents() {
        return sportEvents.size();
    }

    @Override
    public int numSportEventsByPlayer(String playerId) {
        Player player = getPlayer(playerId);
        return (player != null) ? player.numEvents() : 0;
    }

    @Override
    public int numPlayersBySportEvent(String sportEventId) {
        SportEvent sportEvent = getSportEvent(sportEventId);
        return (sportEvent != null) ? sportEvent.numPlayers() : 0;
    }

    @Override
    public int numSportEventsByOrganizingEntity(String orgId) {
        OrganizingEntity organizingEntity = getOrganizingEntity(orgId);
        return (organizingEntity != null) ? organizingEntity.numEvents() : 0;
    }

    @Override
    public int numSubstitutesBySportEvent(String sportEventId) {
        SportEvent sportEvent = getSportEvent(sportEventId);
        return (sportEvent != null) ? sportEvent.numSubstitutes() : 0;
    }

    @Override
    public Player getPlayer(String playerId) {
        return (players.containsKey(playerId)) ? players.get(playerId) : null;
    }

    @Override
    public SportEvent getSportEvent(String eventId) {
        return (sportEvents.containsKey(eventId)) ? sportEvents.get(eventId) : null;
    }

    @Override
    public OrganizingEntity getOrganizingEntity(String id) {
        return (organizingEntities.containsKey(id)) ? organizingEntities.get(id) : null;
    }

    @Override
    public File currentFile() {
        return (files.size() > 0 ? files.peek() : null);
    }

    @Override
    public int numRoles() {
        return this.numRoles;
    }

    @Override
    public Role getRole(String roleId) {
        for(int i=0; i<numRoles; i++){
            if(roles[i].getId().equals(roleId)) return roles[i];
        }
        return null;
    }

    @Override
    public int numWorkers() {
        return workers.size();
    }

    @Override
    public Worker getWorker(String dni) {
        return (workers.containsKey(dni)) ? workers.get(dni) : null;
    }

    @Override
    public int numWorkersByRole(String roleId) {
        Role role = getRole(roleId);
        return (role != null) ? role.numAssignedWorkers() : 0;
    }

    @Override
    public int numWorkersBySportEvent(String sportEventId) {
        SportEvent sportEvent = getSportEvent(sportEventId);
        return (sportEvent != null) ? sportEvent.numWorkers() : 0;
    }

    @Override
    public int numRatings(String playerId) {
        Player player = getPlayer(playerId);
        return (player != null) ? player.getNumRatings() : 0;
    }

    @Override
    public int numAttenders(String sportEventId) {
        SportEvent sportEvent = getSportEvent(sportEventId);
        return (sportEvent != null) ? sportEvent.numAttenders() : 0;
    }

    @Override
    public int numFollowers(String playerId) {
        Player player = getPlayer(playerId);
        return (player != null) ? player.getNumFollowers() : 0;
    }

    @Override
    public int numFollowings(String playerId) {
        Player player = getPlayer(playerId);
        return (player != null) ? player.getNumFollowing() : 0;
    }
}
