package uoc.ds.pr.util;

import java.util.AbstractMap;

public class JsonUtil {
    public static String json(AbstractMap.SimpleEntry<String, String>... entries){
        String json = "{";
        for (AbstractMap.SimpleEntry<String, String> entry : entries){
            String pair = "'{key}': '{value}', ";
            pair = pair.replace("{key}",entry.getKey());
            pair = pair.replace("{value}",entry.getValue());
            json = json.concat(pair);
        }
        json = json.concat("end");
        return json.replace(", end","}");
    }
}
