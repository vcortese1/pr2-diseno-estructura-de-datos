package uoc.ds.pr.util;

import uoc.ds.pr.SportEvents4Club;

public class LevelHelper {

    public static SportEvents4Club.Level getLevel(int ratings){
        if (ratings<2) return SportEvents4Club.Level.ROOKIE;
        else if (ratings >=2 && ratings < 5) return SportEvents4Club.Level.PRO;
        else if (ratings >= 5 && ratings < 10) return SportEvents4Club.Level.EXPERT;
        else if (ratings >= 10 && ratings < 15) return SportEvents4Club.Level.MASTER;
        else return SportEvents4Club.Level.LEGEND;
    }
}
