package uoc.ds.pr.model;

import uoc.ds.pr.SportEvents4Club;
import uoc.ds.pr.util.JsonUtil;

import java.util.AbstractMap;

public class Rating extends Post{
    private SportEvents4Club.Rating rating;
    private String message;
    private Player player;

    private String eventId;

    public Rating(SportEvents4Club.Rating rating, String message, Player user, String eventId) {
        this.rating = rating;
        this.message = message;
        this.player = user;
        this.eventId = eventId;
    }

    public SportEvents4Club.Rating rating() {

        return this.rating;
    }

    public Player getPlayer() {
        return this.player;
    }

    @Override
    public String message() {
        return JsonUtil.json(
                new AbstractMap.SimpleEntry<>("player",this.player.getId()),
                new AbstractMap.SimpleEntry<>("sportEvent",this.eventId),
                new AbstractMap.SimpleEntry<>("rating",this.rating.name()),
                new AbstractMap.SimpleEntry<>("action","rating")
        );
    }
}
