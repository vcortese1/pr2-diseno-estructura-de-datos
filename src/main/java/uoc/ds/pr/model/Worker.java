package uoc.ds.pr.model;

import java.time.LocalDate;

public class Worker {
    private String dni;
    private String name;
    private String surname;
    private LocalDate birthday;
    private Role role;

    public Worker(String dni, String name, String surname, LocalDate birthday, Role role) {
        this.dni = dni;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.role = role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public boolean setRole(Role role) {
        Boolean hasChanged = (!this.role.equals(role));
        this.role = role;
        return hasChanged;
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getRoleId() {
        return role.getId();
    }

    public Role getRole() {
        return role;
    }
}
