package uoc.ds.pr.model;

import uoc.ds.pr.util.JsonUtil;

import java.util.AbstractMap;
import java.util.Comparator;

public class Enrollment extends Post{

    public static final Comparator<Enrollment> SUBTITUTES_PRIORITY = (enrollment1, enrollment2)-> enrollment1.compareTo(enrollment2);
    Player player;
    boolean isSubstitute;

    String eventId;

    public Enrollment(Player player, boolean isSubstitute, String eventId) {
        this.player = player;
        this.isSubstitute = isSubstitute;
        this.eventId = eventId;
    }

    public Player getPlayer() {
        return player;
    }

    public int compareTo(Enrollment inputE){
        return (this.player.getLevel().getValue() < inputE.player.getLevel().getValue()) ? -1 : 0;
    }

    @Override
    public String message() {
        return JsonUtil.json(
                new AbstractMap.SimpleEntry<>("player",this.player.getId()),
                new AbstractMap.SimpleEntry<>("sportEvent",this.eventId),
                new AbstractMap.SimpleEntry<>("action","signup")
        );
    }
}
