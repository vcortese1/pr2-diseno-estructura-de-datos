package uoc.ds.pr.model;

import edu.uoc.ds.adt.helpers.Position;
import edu.uoc.ds.adt.sequential.LinkedList;
import edu.uoc.ds.adt.sequential.List;
import edu.uoc.ds.traversal.Iterator;
import edu.uoc.ds.traversal.Traversal;


public class Role {

    private String id;
    private String description;
    private List<Worker> assignedWorkers;

    public Role(String id, String name) {
        this.id = id;
        this.description = name;
        this.assignedWorkers = new LinkedList<>();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Iterator<Worker> getAssignedWorkers() {
        return assignedWorkers.values();
    }

    public boolean hasAssignedWorkers(){
        return assignedWorkers.size() > 0;
    }

    public void addWorkerToRole(Worker worker){
        assignedWorkers.insertEnd(worker);
    }

    public void removeFormAssignedWorkers(Worker worker){

        for (Traversal i = assignedWorkers.positions(); i.hasNext(); ){
            Position pos = i.next();
            Worker workerIt = (Worker) pos.getElem();
            if (workerIt.getDni().equals(worker.getDni())) assignedWorkers.delete(pos);
        }
    }

    public void replaceWorkerRole(Worker worker, Role lastRole){
        assignedWorkers.insertEnd(worker);
        lastRole.removeFormAssignedWorkers(worker);
    }

    public boolean equals(Role role){
        return this.id.equals(role.id);
    }

    public int numAssignedWorkers(){
        return assignedWorkers.size();
    }
}
