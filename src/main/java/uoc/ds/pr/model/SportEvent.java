package uoc.ds.pr.model;

import edu.uoc.ds.adt.nonlinear.HashTable;
import edu.uoc.ds.adt.nonlinear.PriorityQueue;
import edu.uoc.ds.adt.sequential.LinkedList;
import edu.uoc.ds.adt.sequential.List;
import edu.uoc.ds.adt.sequential.Queue;
import edu.uoc.ds.adt.sequential.QueueArrayImpl;
import edu.uoc.ds.traversal.Iterator;
import uoc.ds.pr.SportEvents4Club;
import uoc.ds.pr.util.OrderedVector;

import java.time.LocalDate;
import java.util.Comparator;

import static uoc.ds.pr.SportEvents4Club.MAX_NUM_ENROLLMENT;
import static uoc.ds.pr.model.Enrollment.SUBTITUTES_PRIORITY;

public class SportEvent implements Comparable<SportEvent> {
    public static final Comparator<SportEvent> CMP_V = (se1, se2)->Double.compare(se1.rating(), se2.rating());
    public static final Comparator<String> CMP_K = (k1, k2)-> k1.compareTo(k2);

    private String eventId;
    private String description;
    private SportEvents4Club.Type type;
    private LocalDate startDate;
    private LocalDate endDate;
    private int max;

    private File file;

    private List<Rating> ratings;
    private double sumRating;

    private Queue<Enrollment> enrollments;

    private PriorityQueue<Enrollment> subtitutesEnrollments;

    private HashTable<String, Attender> attenders;

    private List<Worker> workers;

    public SportEvent(String eventId, String description, SportEvents4Club.Type type,
                      LocalDate startDate, LocalDate endDate, int max, File file) {
        setEventId(eventId);
        setDescription(description);
        setStartDate(startDate);
        setEndDate(endDate);
        setType(type);
        setMax(max);
        setFile(file);
        this.enrollments = new QueueArrayImpl<>(MAX_NUM_ENROLLMENT);
        this.subtitutesEnrollments = new PriorityQueue<>(MAX_NUM_ENROLLMENT, SUBTITUTES_PRIORITY);
        this.attenders = new HashTable<>();
        this.ratings = new LinkedList<>();
        this.workers = new LinkedList<>();
    }


    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SportEvents4Club.Type getType() {
        return type;
    }

    public void setType(SportEvents4Club.Type type) {
        this.type = type;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }


    public double rating() {
        return (this.ratings.size()>0?(sumRating / this.ratings.size()):0);
    }

    public Rating addRating(SportEvents4Club.Rating rating, String message, Player player) {
        Rating newRating = new Rating(rating, message, player, this.eventId);
        ratings.insertEnd(newRating);
        sumRating+=rating.getValue();
        return newRating;
    }

    public boolean hasRatings() {
        return ratings.size()>0;
    }

    public Iterator<Rating> ratings() {
        return ratings.values();
    }

    public void addEnrollment(Player player, String eventId) {
        Enrollment enrollment = new Enrollment(player, false, eventId);
        enrollments.add(enrollment);
        player.addPost(enrollment);
    }

    public void addEnrollmentAsSubtitute(Player player, String eventId) {
        Enrollment enrollment = new Enrollment(player, true, eventId);
        subtitutesEnrollments.add(enrollment);
        player.addPost(enrollment);
    }

    public boolean is(String eventId) {
        return this.eventId.equals(eventId);
    }

    @Override
    public int compareTo(SportEvent se2) {
        return this.getEventId().compareTo(se2.getEventId());
        //return Double.compare(rating(), se2.rating() );
    }

    public boolean isFull() {
        return (attenders.size() + enrollments.size()) >= max;
    }

    public int numPlayers() {
        return enrollments.size() + subtitutesEnrollments.size();
    }

    public int numSubstitutes(){
        return subtitutesEnrollments.size();
    }

    public int numWorkers(){
        return workers.size();
    }

    public int getNumSubstitutes() {
        return subtitutesEnrollments.size();
    }

    public Iterator<Worker> getWorkers() {
        return workers.values();
    }

    public boolean hasWorker(String dni) {
        Iterator<Worker> iterator = getWorkers();
        while (iterator.hasNext()) {
            Worker candidate = iterator.next();
            if (candidate.getDni().equals(dni))
                return true;
        }
        return false;
    }

    public void addWorker(Worker worker){
        workers.insertEnd(worker);
    }

    public boolean hasWorkers(){
        return workers.size() > 0;
    }

    public Iterator<Enrollment> getSubtitutesEnrollments() {
        OrderedVector<Enrollment> orderedVector = new OrderedVector<>(subtitutesEnrollments.size(), SUBTITUTES_PRIORITY);
        for (Iterator i = subtitutesEnrollments.values(); i.hasNext(); ){
            Enrollment enrollment = (Enrollment) i.next();
            orderedVector.update(enrollment);
        }
        return orderedVector.values();
    }

    public boolean hasAttender(String phoneNumber){
        return attenders.containsKey(phoneNumber);
    }

    public boolean hasAttenders(){
        return attenders.size() > 0;
    }

    public void addAttender(Attender attender){
        attenders.put(attender.getPhoneNumber(), attender);
    }

    public Attender getAttender(String phoneNumber){
        return (hasAttender(phoneNumber)) ? attenders.get(phoneNumber) : null;
    }

    public Iterator<Attender> getAttenders() {
        return attenders.values();
    }

    public OrganizingEntity getOrganizingEntity(){
        return this.file.getOrganization();
    }

    public int numAttenders(){
        return attenders.size();
    }
}
