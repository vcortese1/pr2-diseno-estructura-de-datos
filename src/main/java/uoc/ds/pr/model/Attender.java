package uoc.ds.pr.model;

public class Attender {
    private String phoneNumber;
    private String name;

    public Attender(String phoneNumber, String name) {
        this.phoneNumber = phoneNumber;
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
