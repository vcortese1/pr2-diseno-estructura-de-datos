package uoc.ds.pr.model;

import edu.uoc.ds.adt.sequential.LinkedList;
import edu.uoc.ds.adt.sequential.List;
import edu.uoc.ds.traversal.Iterator;
import uoc.ds.pr.SportEvents4Club;
import uoc.ds.pr.util.LevelHelper;

import java.time.LocalDate;
import java.util.Comparator;

public class Player implements Comparable<Player>{
    public static final Comparator<Player> CMP_BY_ID = (p1, p2)-> p1.getId().compareTo(p2.getId());
    private String id;
    private String name;
    private String surname;
    private List<SportEvent> events;
    private LocalDate birthday;
    private Integer numRatings;
    private List<Post> posts;
    private List<Rating> ratings;
    private SportEvents4Club.Level level;
    private int numFollowers; // número de jugadores que siguen a este jugador
    private int numFollowing; // número de jugadores a los que sigue este jugador

	public Player(String idUser, String name, String surname, LocalDate birthday) {
        this.setId(idUser);
        this.setName(name);
        this.setSurname(surname);
        this.setBirthday(birthday);
        this.events = new LinkedList<>();
        this.numRatings = 0;
        this.posts = new LinkedList<>();
        this.ratings = new LinkedList<>();
        this.level = LevelHelper.getLevel(numRatings);
        this.numFollowers = 0;
        this.numFollowing = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }
    public boolean is(String playerID) {
        return id.equals(playerID);
    }

    public void addEvent(SportEvent sportEvent) {
        events.insertEnd(sportEvent);
    }

    public int numEvents() {
        return events.size();
    }

    public boolean isInSportEvent(String eventId) {
        boolean found = false;
        SportEvent sportEvent = null;
        Iterator<SportEvent> it = getEvents();
        while (it.hasNext() && !found) {
            sportEvent = it.next();
            found = sportEvent.is(eventId);
        }
        return found;
    }

    public int numSportEvents() {
        return events.size();
    }

    public Iterator<SportEvent> getEvents() {
        return events.values();
    }

    public boolean hasEvents() {
        return this.events.size()>0;
    }

    public Integer getNumRatings() {
        return numRatings;
    }

    public void addRating(Rating rating){
        this.ratings.insertEnd(rating);
        this.numRatings++;
        this.level = LevelHelper.getLevel(numRatings);
        posts.insertEnd(rating);
    }

    public SportEvents4Club.Level getLevel() {
        return level;
    }

    public void addFollower(){
        numFollowers++;
    }

    public void addFollowing(){
        numFollowing++;
    }

    public int getNumFollowers() {
        return numFollowers;
    }

    public int getNumFollowing() {
        return numFollowing;
    }

    @Override
    public int compareTo(Player o) {
       return this.getId().compareTo(o.getId());
    }

    public boolean isFollower(Iterator<Player> followers){
        for(Iterator<Player> i = followers; i.hasNext();){
            Player p = i.next();
            if(this.compareTo(p) == 0) return true;
        }
        return false;
    }

    public Iterator<Post> getPosts() {
        return posts.values();
    }

    public void addPost(Post post){
        this.posts.insertEnd(post);
    }

    public boolean hasPosts(){
        return posts.size()>0;
    }
}
