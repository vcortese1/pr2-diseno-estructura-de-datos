DS - PR2
____________________________________________

Gitlab: https://gitlab.com/vcortese1/pr2-diseno-estructura-de-datos.git

Sobre la solución propuesta por el profesor se han añadido los siguientes recursos:

Nuevo alcance:
- Modelos de entidades: main/java/uoc/ds/pr/model
    - Attender: Modelo que implementa los asistentes a un evento.
    - Post: Clase abstracta que extienden Enrollment y Rating
    - Role: Modelo que implementa los roles.
    - Worker: Modelo que implementa los trabajadores.
- JsonUtil: main/java/uoc/ds/pr/util/JsonUtil.java
    - Permite gestionar las salidas en formato JSON de los post.
- SportEvents4ClubImpl: main/java/uoc/ds/pr/SportEvents4ClubImpl.java
    - Implementa la interfaz propuesta, así como las distintas estructuras para gestionar la aplicación siguiendo los modelos propuestos en la solución de la PEC1.
- Excepciones: /main/java/uoc/ds/pr/exceptions
    - Implementa todas las excepciones definidas en la propuesta de la práctica relacionadas con los nuevos requerimientos.

Capturas ejecución de los test:

SportEvents4ClubPR1Test
![SportEvents4ClubPR1Test](/src/main/java/uoc/ds/pr/resultados_ejecucion/pr2_1.PNG)

SportEvents4ClubPR2Test
![SportEvents4ClubPR2Test](/src/main/java/uoc/ds/pr/resultados_ejecucion/pr2_2.PNG)

SportEvents4ClubPR2TestPlus
![SportEvents4ClubPR2TestPlus](/src/main/java/uoc/ds/pr/resultados_ejecucion/pr2_3.PNG)

